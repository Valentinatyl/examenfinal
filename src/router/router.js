const express = require("express");
const item = require("../models/Itemmodel.js");

const rutas = express.Router();


const connectDB = require("../conection/db.js");
connectDB();


rutas.get("/item", async (req, res) => {
    try {

        const traernombre = await item.find();
        res.json(traernombre);
    } catch (err) {
        res.json("No hay registros en su base de datos")
    }
});





rutas.post("/item", async (req, res) => {
    try {

        const { nombre, descripcion } = req.body
        const nuevoRegistro = new item({
            nombre,
            descripcion
        
        });
        nuevoRegistro.nombre = nombre;
        nuevoRegistro.descripcion = descripcion;

        await nuevoRegistro.save();
        res.json("Nuevo item  guardado");
        
    }catch (err) {
        res.json("Te falto este dato")
    }
});



rutas.put("/item/:id", async (req, res) => {

     const itemid = req.params.id;
    const {nombre,descripcion} = req.body;
    const itemencontrado = await item.findById(itemid);

    itemencontrado.nombre = nombre;
        itemencontrado.descripcion = descripcion;

    await itemencontrado.save();
    res.json("itemencontrado");

});

rutas.delete("/item/:id", async (req, res) => {

    const itemid = req.params.id;
    const itembuscado = await item.findById(itemid);
   await itembuscado.deleteOne();
    res.json("item eliminado")
    
});


module.exports=rutas; 
