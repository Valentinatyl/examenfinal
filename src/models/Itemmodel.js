const mongoose = require("mongoose");

const Item = new mongoose.Schema({

    nombre: {
        type: String,
        required: true,

    },
    descripcion: {
        type: String,
        required: true,

    },

});

const item = mongoose.model("items",Item);
module.exports = item;